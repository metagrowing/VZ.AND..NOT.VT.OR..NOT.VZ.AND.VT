        SUBROUTINE BILD (DD)
        INTEGER LINES , POINTS
        COMMON /ZZ/ A,B,C,D
C KONSTANTEN
        XMIN=0.0
        XMAX=14.5
        YMIN=-7.25
        YMAX=+7.25
        A=0.05
        B=9.00
        C=-2.0
        D=DD
        LINES=43
        POINTS=191
C NEUER URSPRUNG
C
        DO 1 IY=0,LINES
        Y=YMIN+(YMAX-YMIN)/LINES*IY
        CALL PERSPECT(XMIN,Y,F(XMIN,Y),3)
        DO 1 IX=0,POINTS
        X=XMIN+(XMAX-XMIN)/POINTS*IX
        CALL PERSPECT(X,Y,F(X,Y),NSICHT(X,Y))
1       CONTINUE
C
C
        DO 2 IX=0,LINES
        X=XMIN+(XMAX-XMIN)/LINES*IX
        CALL PERSPECT(X,YMIN,F(X,YMIN),3)
        DO 2 IY=0,POINTS
        Y=YMIN+(YMAX-YMIN)/POINTS*IY
        CALL PERSPECT(X,Y,F(X,Y),NSICHT(X,Y))
2       CONTINUE
        RETURN
        END
