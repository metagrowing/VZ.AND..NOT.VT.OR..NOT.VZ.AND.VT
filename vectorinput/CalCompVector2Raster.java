package vectorinput;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

public class CalCompVector2Raster {
//	private static final int SIZEX = 1024;
//	private static final int SIZEY = SIZEX;
	private static double scale = 50;

//	static String path = "./";
//	static String[] cal = {
//			"fort.100",
//	};

	static List<Line> pl = new ArrayList<Line>();

	public static void main(String[] args) {
		scale = args.length >= 1 ? Double.parseDouble(args[0]) : 50;
		parse_calcomp(args.length >= 2 ? args[1] : "fort.100");
	}

	public static void parse_calcomp(String inputFile) {

		Color col = Color.BLACK;
		double x0 = 0;
		double y0 = 0;
		double mx = 0;
		double my = 0;

		try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	String[] parts = line.split("[ ]+");
		    	try {
					switch(parts[0]) {
					case "NEWPEN":
						switch(Integer.parseInt(parts[1])){
						case 1:
							col = Color.BLACK;
							break;
						case 2:
							col = Color.RED;
							break;
						case 3:
							col = Color.BLUE;
							break;
						}
						break;
					case "PLOT":
						switch(parts[3]) {
						case "-3":
							mx += Double.parseDouble(parts[1]);
							my += Double.parseDouble(parts[2]);
							break;
						case "2":
							double x1 = Double.parseDouble(parts[1]);
							double y1 = Double.parseDouble(parts[2]);
//		    			g.drawLine(x0+mx, y0+my, x1+mx, y1+my);
//		    			g.drawLine(y0+my, x0+mx+SIZEY, y1+my, x1+mx+SIZEY);
//							g.drawLine(SIZEX-(y0+my), SIZEY/2-x0+mx, SIZEX-(y1+my), SIZEY/2-x1+mx);
//							pl.add(new Line(SIZEX-(y0+my), SIZEY/2-x0+mx, SIZEX-(y1+my), SIZEY/2-x1+mx, col));
							pl.add(new Line(y0+my, x0+mx, y1+my, x1+mx, col));
							x0 = x1;
							y0 = y1;
							break;
						case "3":
							x0 = Double.parseDouble(parts[1]);
							y0 = Double.parseDouble(parts[2]);
							break;
						case "999":
							break;
						default:
							System.err.println("unexpected IPEN in PLOT " + parts[0]);
						}
						break;
					default:
						System.err.println("unexpected comand " + parts[0]);
					}
				} catch (Exception e) {
					System.err.println(line);
					e.printStackTrace();
				}
		    }

		    double xmin=Double.MAX_VALUE, xmax=Double.MIN_VALUE;
		    double ymin=Double.MAX_VALUE, ymax=Double.MIN_VALUE;
		    for (Iterator<Line> iterator = pl.iterator(); iterator.hasNext();) {
				Line l = iterator.next();
				if(l.x0 < xmin) xmin = l.x0;
				if(l.x0 > xmax) xmax = l.x0;
				if(l.y0 < ymin) ymin = l.y0;
				if(l.y0 > ymax) ymax = l.y0;
				if(l.x1 < xmin) xmin = l.x1;
				if(l.x1 > xmax) xmax = l.x1;
				if(l.y1 < ymin) ymin = l.y1;
				if(l.y1 > ymax) ymax = l.y1;
			}
		    double xdim = xmax - xmin;
		    double ydim = ymax - ymin;
		    System.out.println(xmin + " " + xmax + " " + ymin + " " + ymax);
		    System.out.println(xdim + " " + ydim);

			toPNG(inputFile, xmin, ymin, xdim, ydim);
			toSVG(inputFile, xmin, ymin, xdim, ydim);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void toPNG(String outputFile, double xmin, double ymin, double xdim, double ydim)
			throws IOException {
		BufferedImage out_iamge = null;
		out_iamge = createBufferdImage((int)(scale*xdim), (int)(scale*ydim));
		Graphics g = out_iamge.getGraphics();
		for (Iterator<Line> iterator = pl.iterator(); iterator.hasNext();) {
			Line l = iterator.next();
			g.setColor(l.col);
//				g.drawLine(xdim/4+l.x0-xmin, ydim/4+l.y0-ymin, xdim/4+l.x1-xmin, ydim/4+l.y1-ymin);
			g.drawLine((int)(scale*xdim)-(int)(scale*(l.x0-xmin)), (int)(scale*ydim)-(int)(scale*(l.y0-ymin)),
					   (int)(scale*xdim)-(int)(scale*(l.x1-xmin)), (int)(scale*ydim)-(int)(scale*(l.y1-ymin)));
		}
		ImageIO.write(out_iamge, "png", new File(outputFile + ".png"));
	}

	private static void toSVG(String outputFile, double xmin, double ymin, double xdim, double ydim)
			throws IOException {
		String head =
				  "<?xml version=\"1.0\" standalone=\"no\"?>\n"
				+ "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \n"
				+ "  \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
				+ "<svg xmlns=\"http://www.w3.org/2000/svg\"\n"
				+ "     version=\"1.1\"\n"
				+ "     width=\"297mm\" height=\"210mm\"\n"
				+ "     viewBox=\"-148 -105 297 210\">\n"
				+ "  <defs>\n"
				+ "    <style type=\"text/css\"><![CDATA[\n"
				+ "      * {\n"
				+ "        fill: none;\n"
				+ "        stroke-width: 1;\n"
				+ "      }\n"
				+ "    ]]></style>\n"
				+ "  </defs>\n"
				+ "  <g transform=\"scale(1, 1)\">";
		String tail =
				  "  </g>\n"
				+ "</svg>";
		Charset charset = Charset.forName("US-ASCII");
		Path path = Path.of(outputFile + ".svg");
		try (BufferedWriter writer = Files.newBufferedWriter(path, charset)) {
		    writer.write(head, 0, head.length());
			for (Iterator<Line> iterator = pl.iterator(); iterator.hasNext();) {
				Line l = iterator.next();
				// <line x1="0" y1="80" x2="100" y2="20" stroke="black" />
				String ls = "<line "
							+ "x1=\"" + ((scale*xdim)-(scale*(l.x0-xmin))) + "\" "
							+ "y1=\"" + ((scale*ydim)-(scale*(l.y0-ymin))) + "\" "
							+ "x2=\"" + ((scale*xdim)-(scale*(l.x1-xmin))) + "\" "
							+ "y2=\"" + ((scale*ydim)-(scale*(l.y1-ymin))) + "\" "
							+ " stroke=\"" + String.format( "#%02X%02X%02X", l.col.getRed(), l.col.getGreen(), l.col.getBlue() ) + "\" />\n";
			    writer.write(ls, 0, ls.length());
			}
		    writer.write(tail, 0, tail.length());
		    writer.close();
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
}

	private static BufferedImage createBufferdImage(int w, int h) {
		BufferedImage out_iamge = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for (int ix = 0; ix < out_iamge.getWidth(); ++ix) {
			for (int iy = 0; iy < out_iamge.getHeight(); ++iy) {
				out_iamge.setRGB(ix, iy, Color.WHITE.getRGB());
			}
		}
		return out_iamge;
	}
}
