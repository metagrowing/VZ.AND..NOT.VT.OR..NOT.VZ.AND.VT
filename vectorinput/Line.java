package vectorinput;

import java.awt.Color;

public class Line {
	final double x0, y0;
	final double x1, y1;
	final Color col;
	public Line(double x0, double y0, double x1, double y1, Color col) {
		super();
		this.x0 = x0;
		this.y0 = y0;
		this.x1 = x1;
		this.y1 = y1;
		this.col = col;
	}
}
