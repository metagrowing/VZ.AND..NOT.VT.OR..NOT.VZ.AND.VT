C-30.0,10.00,20.00
        INTEGER LINES , POINTS
        COMMON AX,AY,AZ
        F(X,Y)=5*SIN(X-4*PI)/(X-4*PI)*SIN(Y)/Y
        PI=3.14156
C KONSTANTEN
        XMIN=0.0
        XMAX=8*PI
        YMIN=-4*PI
        YMAX=+4*PI
        LINES=29
        POINTS=101
C NEUER URSPRUNG
        CALL PLIDF ('THOMAS  ')
        CALL NEWPEN(3)
        CALL PLOT(5.,15.,-3)
        READ (2,10) AX,AY,AZ
10      FORMAT(X,F5.1X,F5.1,X,F5.1)
C
C
        DO 1 IY=0,LINES
        Y=YMIN+(YMAX-YMIN)/LINES*IY
        CALL PERSPECT(XMIN,Y,F(XMIN,Y),3)
        DO 1 IX=0,POINTS
        X=XMIN+(XMAX-XMIN)/POINTS*IX
        CALL PERSPECT(X,Y,F(X,Y),2)
1       CONTINUE
C
C
        DO 2 IX=0,LINES
        X=XMIN+(XMAX-XMIN)/LINES*IX
        CALL PERSPECT(X,YMIN,F(X,YMIN),3)
        DO 2 IY=0,POINTS
        Y=YMIN+(YMAX-YMIN)/POINTS*IY
        CALL PERSPECT(X,Y,F(X,Y),2)
2       CONTINUE
C
        CALL PLOT (30.,0.,3)
        CALL PLOT (30.,0.,999)
        STOP
        END
