# VZ.AND..NOT.VT.OR..NOT.VZ.AND.VT

These are FORTRAN IV programs for plotting mathematical functions.
A wire frame is drawn in central projection with “hidden line” removal.
The FORTRAN source code is from 1982. Made this at Fachhochschule Ulm during my studies.

These programs are still working on a i9-9900K CPU running Debian 10.
The old FORTRAN IV source code can be compiled with the GNU Fortran compiler.
But now I don't have access to a CalComp plotter.
So all NEWPEN and PLOT calls are logged to a file.
A Java program later interprets this file to produce Portable Network Graphics.

## Sytem requirements in 1982
The original program runs on a MODCOMP mainframe build by a company called modular computer systems.
This computer was connected to a CalComp drum plotter.

## Current Sytem requirements
- GNU gfortan Compiler
- openjdk-11-jre

## Documentation
- The f.f file in folder sin_sicht contains the function to plot. Edit this file to have your own function.

- Compile all FORTRAN stuff to a.out. This will be the executable.

- Edit `fort.2` It contains the parameters.

- Run `a.out`. It reads its parameters from channel 2 (`fort.2`) and writes plot commands to channel 100 (`fort.100`).

- Use CalCompVector2Raster to convert the content of `fort.100` to a PNG image and SVG.

## Shell scripts

#### Building the CalComp to PNG and SVG converter.
```shell
#!/bin/bash
set -x
javac vectorinput/Line.java vectorinput/CalCompVector2Raster.java
set +x
```

### Converting fort.100 files to PNG and SVG
Sample `fort.100` files are already in this repository.

First you had to compile the CalCompVector2Raster tool. This is Java source code.

Do this in the top level folder of this project.

```shell
javac vectorinput/Line.java vectorinput/CalCompVector2Raster.java
```

Switch to the folder containing the `fort.100` file.
```shell
java -cp .. vectorinput/CalCompVector2Raster
```

This will generate to new Files: `fort.100.png` and `fort.100.svg`.

### After editing a FORTRAN file: Compile, run and convert to PNG and SVG.
First switch to the folder containing the FORTRAN files.

Then execute this bash script.
```shell
#!/bin/bash
set -x
gfortran -g ../CALCOMP/*.f *.f
./a.out
java -cp .. vectorinput/CalCompVector2Raster
set +x
```

## Scan of an original plot
[3D from 1982](https://metagrowing.org/?page_id=249)

![Superimposition of two functions](ueber_sicht/ueber-plot.jpg "Superimposition of two functions")

## Links
[MODCOMP](https://en.wikipedia.org/wiki/MODCOMP)

[CalComp Software Reference Manual](http://bitsavers.informatik.uni-stuttgart.de/pdf/calcomp/CalComp_Software_Reference_Manual_Oct76.pdf)

[GNU Fortran: Using the compiler](https://gcc.gnu.org/wiki/GFortranUsage)
